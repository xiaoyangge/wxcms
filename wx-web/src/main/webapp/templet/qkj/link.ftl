<div class="layui-row">
    <div class="layui-col-md12">
        <div class="link-title">
            <h2>友情链接</h2>
        </div>
    </div>
    <div class="layui-col-md12">
        <hr>
    </div>

    <div class="layui-col-md12">
    <#if linklist ? exists>
        <#list linklist as link>
            <#if link_index lte 3>
                <div class="layui-col-md3">
                    <a href="${link.link_address}" target="_blank" class="link">
                        <div class="link-img">
                            <img src="${link.link_images}">
                        </div>
                        <div class="link-name">${link.link_title}</div>
                    </a>
                </div>
            </#if>
        </#list>
    </#if>
    </div>

    <div class="layui-col-md12">
    <#if linklist ? exists>
        <#list linklist as link>
            <#if link_index gte 4>
                <div class="layui-col-md3">
                    <a href="${link.link_address}" target="_blank" class="link">
                        <div class="link-img">
                            <img src="${link.link_images}">
                        </div>
                        <div class="link-name">${link.link_title}</div>
                    </a>
                </div>
            </#if>
        </#list>
    </#if>
    </div>
</div>